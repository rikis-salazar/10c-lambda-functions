#include <iostream>
#include <functional>
#include <algorithm>
#include <numeric>

int main(){
    using namespace std;

    // Q: So, what exactly does a lambda function define?
    // A: It defines an anonymous function object.

    // Here is a simple example:
    [](){}();
    // Wait! Whaaaaaaat???
    
    []  // The 'lambda introducer' (it specifies the type of 'capture')
    ()  // The 'lambda declarator' (it is used to declare the parameters)
    {}  // The body of the function (it [usually] does not require a return)
    ()  // Acts like a call to operator() 
    ;   // The end of the statement


    // Here is a more complex example
    int n = 10;
    double x;
    auto fun = [ =,&x ] (double d, int k) {
                   cout << "d = " << (x = d) << ", k = " << k << '\n';
                   return x += d;
               }; 

    fun(0,n);
    cout << "x = " << x << '\n';
    // Wait! Whaaaaaaat???

    // Confuse yet? If that is the case you should first read the handout, and
    // then come back to this file later.

    return 0;
}
