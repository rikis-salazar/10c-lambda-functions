#include <iostream>
#include <functional>
#include <algorithm>
#include <numeric>


class Predicate {
    public:
        bool operator()( int x ) {
            return x % 2 == 0;
        }
} is_even;



class BinaryPredicate {
    public:
        bool operator()( int x, int y ) {
            return x < y ;
        }
} first_less_than_second;


class MyBinaryFunction {
    public:
        int operator()( int x, int y ) {
            return x + y + 1;
        }
} generous_addition;



int add_first_and_second( int x, int y ) {
    return x + y;
}




int main(){

    std::vector<int> v = { 4, 4, 8, 23, 2017, 4, 5, 2 };

    // Say we want to print our vector...
    std::for_each( 
        // The range
            std::begin(v), 
            std::end(v),
        // The action: a lambda function defined in-place
            []( int x ){ std::cout << x << '-' ; }
        );
    std::cout << '\n';


    // ... or triple its values ...
    std::for_each( 
            v.rbegin(), 
            v.rend(),
            []( int& x ){ x *= 3 ; }
        );

    
    // ... and get ready to display the vector...
    auto print_vector = []( const std::vector<int>& a ){
        for ( auto x : a )
            std::cout << x << "  ";
        std::cout << '\n';
    };

    // ... over ...
    print_vector(v);

    // ... and over ...
    print_vector(v);

    // ... and over.
    print_vector(v);



    // I do not like even numbers... let's find them and kill them
    auto iter = std::begin(v);
    auto last = std::end(v);
    while ( iter != last ){
        iter = std::find_if(iter,last, is_even );
        // iter = find_if(iter,last, !is_even );    // <-- Does not make sense
        if ( iter != last )  // Redundant???
            *iter++ = 0;
    }
    print_vector(v);

    
    // I also do not like big numbers...
    //
    // We can
    //     - Scroll up, then declare/define a functor (c.f. is_even )
    //     - Create a lambda in-place
    //     - Use a binder ( more on this later )


    // Let's use the lambda approach. However, first we have to answer the
    // question of what is considered to be 'big'? How about 1000? Big enough?
    iter = std::begin(v);
    last = std::end(v);
    while ( iter != last ){
        iter = std::find_if(iter,last, []( int x ){ return x > 1000; } );
        if ( iter != last ) 
            *iter++ = 0;
    }
    print_vector(v);


    
    // Now the vector is sad (mostly zeros). Let us fix this via a
    // binder + wrapper combo.
    auto add_one = std::bind1st( std::plus<int>(), 1 );
    std::transform( std::begin(v), std::end(v), std::begin(v), add_one );
    print_vector(v);



    // But, don't we have similar functions already ???
    // How about we try our add_first_and_second with 1 as one of the
    // parameters?
    //
    //     std::function<int(int)> add_one_again = 
    //             std::bind1st( add_first_and_second, 1 );
    //             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    //                        IT FAILS!!!
    // However we can wrap add_first_and_second via std::ptr_fun.

    auto add_one_again = std::bind1st( std::ptr_fun(add_first_and_second), 1 );
    std::transform( std::begin(v), std::end(v), std::begin(v), add_one_again );
    print_vector(v);
   

    
    // Turns out bind1st [ or bind2nd for that matter ] not only are are not
    // good at handling non-standard functions without help (c.f. std::ptr_fun),
    // but also, they are not very flexible. Instead one can use the newer
    // classes std::bind, and std::function.
    //
    // Here we use both of them to wrap and use our generous_addition functor.
    // Note that since the extra 1 is added within operator(), we have to fix
    // [bind] the value 0 instead.
    std::function<int(int)> add_one_yet_again = 
            std::bind( generous_addition, 0, std::placeholders::_1 );
    //                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
    //                  [Don't worry. We'll explain this later.]

    std::transform( v.begin(), v.end(), v.begin(), add_one_yet_again );
    print_vector(v);



    // Notice that a simple lambda function is better suited to handle the task
    // of adding one to every element of the vector.
    std::transform( v.begin(), v.end(), v.begin(), [](int x){return x+1;} );
    print_vector(v);


    // Alternatively, we can use std::for_each and a reference argument
    std::for_each( v.begin(), v.end(), []( int& x ){ x += 1; } );
    print_vector(v);


    // To conclude this example, let us find the last element bigger than 10 
    // using our first_less_than_second functor.
    auto pos = std::find_if( v.rbegin(), v.rend(),
            std::bind(first_less_than_second, 10 , std::placeholders::_1 ) );

    std::cout << "The last element in v that is bigger than 10 is "
              << *pos << ".\n";

    
    return 0;
}

/**

    OUTPUT:
 
        4-4-8-23-2017-4-5-2-
        12  12  24  69  6051  12  15  6  
        12  12  24  69  6051  12  15  6  
        12  12  24  69  6051  12  15  6  
        0  0  0  69  6051  0  15  0  
        0  0  0  69  0  0  15  0  
        1  1  1  70  1  1  16  1  
        2  2  2  71  2  2  17  2  
        3  3  3  72  3  3  18  3  
        4  4  4  73  4  4  19  4  
        5  5  5  74  5  5  20  5  
        The last element in v that is bigger than 10 is 20.

**/
