# Lambda functions

As we have seen in previous examples (e.g., final remarks in the [generic
algorithms handout][gen-alg-repo]), lambda functions allow us to write un-named
functions _in place_. This, in turn make the _Standard Template Library_
algorithms much more usable. One more thing in favor of lambda functions is the
fact that variables/objects from local _context_ can be 'captured' and used in
the lambda function without them having to enter the lambda as parameters.

[gen-alg-repo]: https://bitbucket.org/rikis-salazar/10c-generic-algorithms

## The basics

A typical lambda function has the following form

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
[capture list](parameter list){function body}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

The capture and parameter lists, as well as the function body can, in fact, be
empty. Thus, `[](){}` is a perfectly valid, although useless, lambda function.
Here is a slightly more useful one:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
[](){ std::cout << "Hello, lambda world!\n"; }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

The parameter list is just like a regular function parameter list. The same is
true for the function body; moreover, any return value is possible. As you can 
see, in both of the previous examples, there is no `return` statement. In these
cases the return type is assumed to be `void`. In the case where there is only
one `return` statement in the function body, the return type us assumed to be
the same as the type of the value being returned.

For example, with this lambda the return type is assumed to be `void`. Moreover,
since the lambda is defined "on the fly", it is ready for us to use it right
away.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
[](){ std::cout << "Hello (again?), lambda world!\n"; }();
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

As you can expect, the parenthesis at the end of the lambda definition give us a
clue that lambdas are either _functors_ or _function pointers_. In the example
below, the compiler has no issue figuring out that the return type is `bool`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
if ( [](int x, int y){ return y == x*x; }(-2,4) )
    std::cout << "In the parabola!\n";
else
    std::cout << "Elsewhere!\n";
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

As you can see, not having to indicate the return type results in lambda
definitions that are very small in most cases. This is one reason they are
particularly well suited to be companions of generic algorithms. 

In the event that the compiler cannot figure out the return type (e.g.,
ambiguitiy, or too many points of return), or in the case we want to perform a
conversion, we can use a special syntax borrowed from elsewhere in the language.
For example, here we tell the compiler that an `int` is needed even though the
operation in the return statement results in a `double`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
std::cout << "This lambda function returns "
          << [](double a, double b) -> int { return a/b; }(4,3) << '\n';
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

The output in this case is `This lambda function returns 1`.


## Capturing the context

The real super power of a lambda function is its ability to store information
about variables that are in the local block scope where the lambda function is
created. Moreover, the lambda function can refer to those variables using the
same name as in the surrounding scope. This is similar to the effect one gets
when creating nested scopes via curly braces.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void some_fun() {
    int outside_int = 3;
    double outside_double = 0.14;
    {
        int inside_int = 2017;
        std::cout << "In " << inside_int << ", pi's value is "
                  << outside_int + outside_double << ".\n";
    }
    
    // The variable `inside_int` is not visible in this outer scope
    //     std::cout << "In " << inside_int << ", pi's value is "  // <-- Oops!
    //               << outside_int + outside_double << ".\n";
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Notice how even though `outside_int` and `outside_double` are not inside the
inner scope, they are visible therein. This effect is not possible if we have
functions instead of blocks delimited by curly braces, as in `C++` a function
cannot be defined within another function. However, upon creation, lambda
functions are allowed to _capture_ these outer variables. The result is a
similar effect to the one detailed above with the nested scopes.

For example the lambda below reproduces the behavior from the previous example.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void some_fun(){
    int outside_int = 3;
    double outside_double = 0.14;
    [outside_int, outside_double](){
        int inside_int = 2017;
        std::cout << "In " << inside_int << ", pi's value is "
                  << outside_int + outside_double << ".\n";
    }();

    // [inside_int](){ std::cout << "'inside_int' not visible here\n"; }();
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

The obvious question is: How is this possible? Isn't this a case of a [lambda]
function inside another function? The answer to the second question is: No!
The answer to the first question is: a lambda function is implemented like a
function object (functor) which the compiler creates using the same variable
names provided in the capture list. It is actually a pretty cool concept.
Namely, the compiler reads the lambda expression, and then replaces it with code
that declares, creates and initializes, and then calls a function object. This
function object stores the captured variables in member fields, which are
initialized when the function object is created.

For the example above, this is more or less what the compiler provides _under
the hood_.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
void some_fun(){
    int outside_int = 3;
    double outside_double = 0.14;

    // This is where the compiler takes over and works under the hood
    class TheLambda {
      private:
        int outside_int;
        double outside_double;

      public:
        TheLambda( int n, double d ) : outside_int(n), outside_double(d) { }

        void operator() const { // <-- **see note**
            int inside_int = 2017;
            std::cout << "In " << inside_int << ", pi's value is "
                      << outside_int + outside_double << ".\n";
        }
    };

    // The outside values are captured via the constructor
    TheLambda lambda( outside_int, outside_double );

    // Then the lambda is called
    lambda();
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

> Note: The function `operator()` is defined as a `const` member by default. If
> for some reason, you want the lambda to modify the captured value inside the
> lambda function, you need to declare the lambda as `mutable`, as in:  
> `[captured_var]() mutable { /* the code */ }`

Although the member fields of the lambda object have the same names as the
captured variables, they holds copies of these values. In particular, if the
lambda function is used only once, we might be tempted to think that there is
really no difference between these similarly named variables. However, notice
what happens if we _save_ the lambda and attempt to reuse it at a later time.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
int the_int = 2017;
auto some_lambda = [the_int](){
    std::cout << "This lambda has a copy of 'the_int' when created: "
              << the_int << '\n';
}
some_lambda();

// Output:
//     This lambda has a copy of 'the_int' when created: 2017


for ( int i = 0 ; i < 3 ; ++i ){
    the_int += 10;
    some_lambda();
}

// Output:
//     This lambda has a copy of 'the_int' when created: 2017
//     This lambda has a copy of 'the_int' when created: 2017
//     This lambda has a copy of 'the_int' when created: 2017
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

This should come as no surprise. The compiler does its under-the-hood thing
just once, instead of every time the lambda is called. When the lambda is
instantiated it grabs a copy of `the_int` and then loses sight of its value.
The numbers reported correspond to the member field created by the compiler.
This behavior could lead to confusion, and in my opinion saving lambdas for
later reuse goes against the very notion of a lambda function. Given said that,
it is possible to capture a variable in such a way that changes are always
noticed by the lambda. Such capture is called _by reference_. In this case,
when the lambda is created, reference variables are used as member fields.
As in the _capture by value_ case, `operator()` is still a `const` member,
however there is no need to use the `mutable` keyword as in this case the member
field [reference] does not change; instead what changes is the value associated
to the reference. Here is a variation of the example above where `the_int` is
captured by reference.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
int the_int = 2017;
auto some_lambda = [&the_int](){
    std::cout << "This lambda captures 'the_int' by reference: "
              << the_int << '\n';
}

for ( int i = 0 ; i < 3 ; ++i ){
    the_int += 10;
    some_lambda();
}
// Output:
//     This lambda captures 'the_int' by reference: 2027
//     This lambda captures 'the_int' by reference: 2037
//     This lambda captures 'the_int' by reference: 2047
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

To conclude this handout, here is a particular way to compute the factorial
of 5.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp} 
int factorial = 1;
for ( int i = 1 ; i <= 5 ; ++i )
    [&factorial]( int x ){ factorial *= x; }(i);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Notice that even though the lambda is reused, there is no need to store it in
order to perform a capture by reference.

## Some shortcuts

There are two shortcut capture specifications that might come in handy at some
point:

* `[=]` captures **all** variables currently in the local block scope by copy
  of their current values
* `[&]` captures **all** variables currently in the local block scope by
  reference

In my humble opinion, there is no reason to use the `[=]` shortcut, except
perhaps for didactic purposes. I will update this handout if I come across a
scenario where it is actually worth.

--- 

Included files:

|**File**|**Description**|**URL**|
|:------------:|:------------------------------------------------|:------------|
| [`readme.md`][read-me] | _readme_ file (`MarkDown` format).| `N/A` |
| [`readme.md.pdf`][read-me-pdf] | _readme_ file (`pdf` format). | `N/A` |
| [`00-intro.cpp`][00-example] | A simple, and a not-so-simple lambda. | [`cpp.sh/47fjb`][00-url] |
| [`01-mor....cpp`][01-example] | More examples + intro to wrapper/binder. | [`cpp.sh/9c2mt`][01-url] |


[read-me]: readme.md
[read-me-pdf]: readme.md.pdf
[00-example]: 00-intro.cpp
[01-example]: 01-more-examples.cpp

[00-url]: http://cpp.sh/47fjb
[01-url]: http://cpp.sh/9c2mt

